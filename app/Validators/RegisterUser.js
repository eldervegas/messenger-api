'use strict';

class RegisterUser {
  get validateAll() {
    return true;
  }

  get rules () {
    return {
      username: 'required|string',
      email:    'required|email|unique:users,email',
      password: 'required|string|min:6|confirmed'
    };
  }
}

module.exports = RegisterUser;
