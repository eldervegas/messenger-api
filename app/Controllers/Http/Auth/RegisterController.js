'use strict';

const User = use('App/Models/User');
const Mail = use('App/Mail/Registration/ConfirmEmailMail');
const randomString = require('random-string');

class RegisterController {
  async register({ request, auth, response }) {
    const user = await User.create({
      username: request.input('username'),
      email: request.input('email'),
      password: request.input('password'),
      confirmation_token: randomString({ length: 40 })
    });

    await (new Mail(user)).handle();

    return response.status(201).json({
      token: await auth.generate(user),
      user: user.toJSON()
    });
  }

  async confirm({ params, response }) {
    const user = await User.findByOrFail('confirmation_token', params.token);
    user.confirmation_token = null;
    user.is_active = true;
    await user.save();

    return response.status(201).json({
      message: 'Your email address has been confirmed'
    });
  }
}

module.exports = RegisterController;
