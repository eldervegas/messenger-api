'use strict';

class UserLogin {
  get rules () {
    return {
      email:    'required|email',
      password: 'required|string|min:6',
    }
  }
}

module.exports = UserLogin;
