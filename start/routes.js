'use strict';

const Route = use('Route');

Route.get('register/confirm/:token', 'Auth/RegisterController.confirm').as('auth.confirm');

Route.post('login', 'Auth/LoginController.login')
  .validator('UserLogin')
  .as('auth.login');
Route.post('register', 'Auth/RegisterController.register')
  .validator('RegisterUser')
  .as('auth.register');
Route.post('password/reset', 'Auth/ResetPasswordController.reset')
  .validator('ResetPassword')
  .as('password.reset');
Route.post('password/email', 'Auth/ResetPasswordController.sendResetCode')
  .validator('SendResetCode')
  .as('password.send');
