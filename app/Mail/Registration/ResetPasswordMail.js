'use strict';

const Mail = use('Mail');
const Env  = use('Env');

class ConfirmEmailMail {

  constructor(user, token) {
    this.user  = user;
    this.token = token;
  }

  async handle() {
    await Mail.send('emails.password_reset', {user: this.user.toJSON(), token: this.token}, message => {
      message
        .to(user.email)
        .from('dev@cronix.ms')
        .subject('Password reset link')
    });
  }
}

module.exports = ConfirmEmailMail;
