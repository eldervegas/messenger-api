'use strict';

const Mail = use('Mail');
const Env  = use('Env');

class ConfirmEmailMail {

  constructor(user) {
    this.user = user;
  }

  async handle() {
    await Mail.send('emails.confirm_email', this.user.toJSON(), message => {
      message
        .to(this.user.email)
        .from(Env.get('MAIL_USERNAME'))
        .subject('Please confirm your email address');
    })
  }
}

module.exports = ConfirmEmailMail;
