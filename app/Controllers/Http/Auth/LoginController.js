'use strict';

const User = use('App/Models/User');
const Hash = use('Hash');

class LoginController {
  async login({ request, auth, response }) {
    const { email, password } = request.all();
    const user = await User.query()
      .where('email', email)
      .first();

    if (!user) {
      return response.status(400).json({ message: 'Invalid email' });
    }

    if (!await Hash.verify(password, user.password)) {
      return response.status(400).json({ message: 'Invalid password' });
    }

    return response.status(200).json({
      token: await auth.generate(user, {
        username: user.username,
        email: user.email
      }),
      user: user.toJSON()
    });
  }
}

module.exports = LoginController;
