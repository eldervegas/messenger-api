'use strict';

class ResetPassword {
  get rules () {
    return {
      email: 'required|email'
    };
  }
}

module.exports = ResetPassword;
