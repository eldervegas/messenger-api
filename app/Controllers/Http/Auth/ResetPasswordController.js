'use strict';

const User = use('App/Models/User');
const PasswordReset = use('App/Models/PasswordReset');
const Mail = use('App/Mail/Registration/ResetPasswordMail');
const Hash = use('Hash');
const randomString = require('random-string');

class ResetPasswordController {
  async sendResetCode({ request, response }) {
    const user = await User.findByOrFail('email', request.input('email'));
    await PasswordReset.query().where('email', user.email).delete();

    const { token } = await PasswordReset.create({
      email: user.email,
      token: randomString({ length: 40 })
    });

    await (new Mail(user, token)).handle();

    return response.status(200).json({
      message: 'Reset link was sent'
    });
  }

  async reset({ request, response }) {
    const user = await User.findByOrFail('email', request.input('email'));
    await PasswordReset.query()
      .where('email', request.input('email'))
      .where('token', request.input('token'))
      .firstOrFail()
      .delete();

    user.password = await Hash.make(request.input('password'));
    await user.save();

    return response.status(204).json({
      message: 'Password success reset'
    });
  }
}

module.exports = ResetPasswordController;
