'use strict';

class ResetPassword {
  get rules () {
    return {
      token: 'required',
      email: 'required',
      password: 'required|confirmed'
    };
  }
}

module.exports = ResetPassword;
